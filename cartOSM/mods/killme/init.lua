minetest.register_chatcommand("killme", {
	description = "Tuez-vous pour ressusciter au point de spawn",
	func = function(name)
		local player = minetest.get_player_by_name(name)
		if player then
			if minetest.settings:get_bool("enable_damage") then
				player:set_hp(0)
				return true
			else
				for _, callback in pairs(core.registered_on_respawnplayers) do
					if callback(player) then
						return true
					end
				end

				-- There doesn't seem to be a way to get a default spawn pos from the lua API
				return false, "Pas de spawn definit"
			end
		else
			-- Show error message if used when not logged in, eg: from IRC mod
			return false, "Vous devez etre en ligne pour vous tuer !"
		end
	end
})
