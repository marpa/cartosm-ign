
local stuff_string = minetest.settings:get("initial_stuff") or
		"default:pick_steel,default:axe_steel,default:shovel_steel," ..
		"default:torch 99,default:cobble 99"

give_initial_stuff = {
	items = {}
}

function give_initial_stuff.give(player)
	minetest.log("action",
			"Kit de démarrage donné à " .. player:get_player_name())
	local inv = player:get_inventory()
		inv:add_item("main", "default:torch 99")
		inv:add_item("main", "default:pick_diamond")
		inv:add_item("main", "default:axe_diamond")
		inv:add_item("main", "craftguide:book")
		inv:add_item("main", "vehicles:concrete 99")
		inv:add_item("main", "homedecor:shingle_side_terracotta 99")
		inv:add_item("main", "vehicles:road 99")
		inv:add_item("main", "streets:sidewalk 99")
		inv:add_item("main", "moreblocks:circular_saw")
		inv:add_item("main", "streets:asphalt_workshop")
		inv:add_item("main", "streets:sign_workshop")
		inv:add_item("main", "markers:mark 4")
		inv:add_item("main", "markers:land_title_register")
end

function give_initial_stuff.add(stack)
	give_initial_stuff.items[#give_initial_stuff.items + 1] = ItemStack(stack)
end

function give_initial_stuff.clear()
	give_initial_stuff.items = {}
end

function give_initial_stuff.add_from_csv(str)
	local items = str:split(",")
	for _, itemname in ipairs(items) do
		give_initial_stuff.add(itemname)
	end
end

function give_initial_stuff.set_list(list)
	give_initial_stuff.items = list
end

function give_initial_stuff.get_list()
	return give_initial_stuff.items
end

give_initial_stuff.add_from_csv(stuff_string)
if minetest.settings:get_bool("give_initial_stuff") then
	minetest.register_on_newplayer(give_initial_stuff.give)
end
