# LISTE DES COMMANDES


###############
## Interactions

+ /msg  <NomJoueur> <message>
	+ Priv: shout
	+ Envoie le message privé <message> au joueur spécifié

+ /me <action>
	+ Priv: shout
	+ Affiche un message au format "* <votre nom> <action>" dans le tchat

+ /skin
	+ Priv: Non
	+ Ouvre la fenêtre de changement d'apparence (faire F7 pour voir les changements)

+ /killme
	+ Priv: Non
	+ Permet de se tuer

+ /heal <NomJoueur>
	+ Priv: Non
	+ Soigne un joueur spécifique (si pas de nom de joueur, vous soigne)

+ /clearinv
	+ Priv: Non
	+ Vide l'inventaire

+ /pulverize
	+ Priv: Non
	+ Détruit l'objet actuellement sélectionné

+ /giveme <itemstring> [nb]
	+ Priv: give
	+ Donne un certain objet un nombre de fois [nb] (par défaut 1) à vous même

+ /give <NomJoueur> <itemstring> [nb]
	+ Priv: give
	+ Donne un certain objet un nombre de fois [nb] (par défaut 1) au joueur <NomJoueur>


###############
## Déplacements

+ /teleport <x>,<y>,<z>
	+ Priv: teleport
	+ Se téléporter soi-même aux coordonnées <x>,<y>,<z>

+ /teleport <NomJoueur>
	+ Priv: teleport
	+ Se téléporter à la position du joueur <NomJoueur>

+ /teleport <NomJoueur> <x>,<y>,<z>
	+ Priv: bring
	+ Téléporte le joueur <NomJoueur> à la position donnée par <x>,<y>,<z>

+ /teleport <NomJoueur1> <NomJoueur2>
	+ Priv: bring
	+ Téléporte le joueur <NomJoueur1> à la position du joueur <NomJoueur2>

+ /sethome
	+ Priv: home
	+ Enregistre votre position courante comme votre maison

+ /home
	+ Priv: home
	+ Vous téléporte à votre maison

+ /top
	+ Priv: top
	+ Vous téléporte sur le nœud non aérien le plus élevé directement au-dessus de vous

+ /spawn
	+ Priv: spawn
	+ Téléporte le joueur au spawn (uniquement si static_spawnpoint est configuré dans minetest.conf)

+ /setspeed <speed> <NomJoueur>
	+ Priv: setspeed
	+ Définit la vitesse d'un joueur : 1 = normal, 2 = 2 fois la vitessse normale, ... (si pas de nom de joueur, définit votre vitesse)


#############
## Informatif

+ /help
	+ Priv: Non
	+ Affiche une liste des commandes disponibles pour les privilèges que vous disposez sur le serveur

+ /status
	+ Priv: Non
	+ Donne la version du serveur Minetest, depuis quand le serveur est en route en secondes (appelé "uptime"), la liste des joueurs connectés et le message du jour (s'il existe)

+ /mods
	+ Priv: Non
	+ Donne la liste des mods installés sur le serveur

+ /gettime
	+ Priv: Non
	+ Donne l'heure

+ /ping
	+ Priv: Non
	+ Pong!

+ /whatisthis
	+ Priv: Non
	+ Donne la référence de l'objet actuellement utilisé

+ /privs <NomJoueur>
	+ Priv: Non
	+ Liste tous les privilèges accordés au joueur spécifié (si pas de nom de joueur, liste vos privilèges)

+ /extrahb [nb]
	+ Priv: Non
	+  Affiche une barre d'inventaire étendue d'un nombre de cases [nb] (defaut : 8 / max : 23)



#############################
## Administration des joueurs

+ /setpassword <NomJoueur> <motdepasse>
	+ Priv: password
	+ Défini le mot de passe <motdepasse> pour le joueur <NomJoueur>

+ /grant <NomJoueur> <privilege>
	+ Priv: privs
	+ Donne le privilège <privilege> au joueur <NomJoueur>

+ /grant <NomJoueur> all
	+ Priv: privs
	+ Donne tous les privilèges possible au joueur <NomJoueur>

+ /revoke <NomJoueur> <privilege>
	+ Priv: privs
	+ Enlève le privilège <privilege> au joueur <NomJoueur>

+ /broadcast
	+ Priv: broadcast
	+ Diffuse un message à tout le serveur

+ /whois <NomJoueur>
	+ Priv: whois
	+ Donne les informations réseau du joueur spécifié

+ /watch <NomJoueur>
	+ Priv: watch
	+ Voir ce que fait un joueur (en mode spectateur)

+ /unwatch
	+ Priv: watch
	+ Arrêter de voir un joueur et revenir à sa position initiale

+ /kick <NomJoueur> [raison]
	+ Priv: kick
	+ Exclus le joueur <NomJoueur>. Optionnellement le motif du bannissement [raison] peut lui être notifié. Ce texte sera vu par le joueur exclu.

+ /ban
	+ Priv: Non
	+ Montre la liste des joueurs bannis

+ /ban <IP>
	+ Priv: ban
	+ Banni le joueur avec l'ip <IP> (il doitêtre connecté au moment du bannissement et ne pourra plus se reconnecter au serveur)

+ /ban <NomJoueur>
	+ Priv: ban
	+ Banni l'ip du joueur <NomJoueur>

+ /unban <NomJoueur>
	+ Priv: ban
	+ Dé-banni le joueur <NomJoueur>

+ /unban <IP>
	+ Priv: ban
	+ Dé-banni le joueur qui a comme ip <IP>

+ /kill <NomJoueur>
	+ Priv: kill
	+ Tue un joueur spécifique

+ /godmode
	+ Priv: godmode
	+ Active ou desactive le mode 'dieu' (santé et souffle infinis)



############################
## Administration du serveur

+ /time 6:00
	+ Priv: settime
	+ Définit l'heure

+ //replaceinverse air air
	+ Priv: worldedit
	+ Permet de supprimer proprement des blocs (après sélection de la zone dans WorldEdit)

+ /clearobjects
	+ Priv: server
	+ Enlève tous les items à terre et les mobs. NB : cette commande peut faire crasher le serveur ou le faire ralentir fortement pendant une période de 10 à plus de 60 secondes !

+ /rollback_check [range] [secondes]
	+ Priv: rollback
	+ Vérifie quelle est la dernière personne à avoir touchée ce node ou à côté. Par défaut [range]=0 et [secondes]=86400 (24h).

+ /rollback <NomJoueur> [seconds]
	+ Priv: rollback
	+ Annule les actions d'un joueur <NomJoueur> (par défaut [seconds]=60)

+ /shutdown
	+ Priv: server
	+ Arrête le serveur



#############
## MODS BONUS


**fishing**

+ /contest_start <duree>
	+ Priv: server
	+ Lance une compétition de pêche immédiatement (<duree> en secondes)

+ /contest_add <jour> <heure> <minutes> <duree>
	+ Priv: server
	+ Lance une compétition de pêche à un moment précis (<jour> <heure> <minutes> : le début 0=tous les jours, 1=dimanche, 2=lundi, ... / <duree> : durée de la compétition en secondes)

+ /contest_stop
	+ Priv: server
	+ Arrête la compétition de pêche

+ /contest_show
	+ Priv: Non
	+ Affiche le résultat de la compétition de pêche


**mymonths**

+ /weather
	+ Priv: Non
	+ Donne le temps qu'il fait

+ /date
	+ Priv: Non
	+ Donne la date

+ /holidays
	+ Priv: Non
	+ Donne la liste des vacances (fictif)

*admin*
+ /setweather rain,storm,snow,snowstorm,sandstorm,hail
	+ Priv: mymonths
	+ Configure le temps qu'il fait dans la zone

+ /setmonth 1-12
	+ Priv: mymonths
	+ Configure le mois

+ /setday 1-14
	+ Priv: mymonths
	+ Configure le jour


**jail**
configurer avant le centre de la prison et le point de spawn dans le init.lua (lignes 7 et 27)

+ /jail <NomJoueur>
	+ Priv: jail
	+ Mettre un joueur en prison

+ /release <NomJoueur>
	+ Priv: jail
	+ Libérer un joueur de la prison

+ /giveme wardenpick <ou> jail:jailwall <ou> jail:glass <ou> jail:ironbars
	+ Priv: jail
	+ Acquérir la pioche de destruction de la prison, ou les murs de prison, ou les verres de prison, ou les barres de prison


