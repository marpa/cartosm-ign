# CartOSM-IGN

Ce projet contient un subgame (cartOSM) et un monde (CartOSM_modele) permettant d'integrer une carte IGN dans le jeu Minetest (version 5.1).


# cartOSM

- Le subgame integre des mods pre-configures avec les fonctions decrites dans le fichier README.txt
- Pour avoir la liste des commandes, consulter le fichier commandes.md


# CartOSM_modele

Ce monde fonctionne avec le subgame cartOSM et permet d'y ajouter des fonctionnalites : prison, zone du spawn protege, mot de passe a la connexion, admin defini. Plus de details dans CartOSM_modele/README.md
