# CartOSM_modele


Ce monde fonctionne avec le subgame cartOSM et permet d'y ajouter des fonctionnalites : prison, zone du spawn protege, mot de passe a la connexion, admin defini.

Recuperer le dossier *CartOSM_modele* du projet CartOSM-IGN : <http://cartosm.git.lespetitsdebrouillards-na.org>

le copier dans le dossier *worlds* de votre application Minetest et renommer *modele* en nom du quartier (sans espace ni caracteres speciaux).

Decompresser l'archive de l'IGN et copier 
- les dossiers *ignfab* et *respawn* (contenus dans *worldmods*) et les coller dans celui du monde (*CartOSM_quartier/worldmods/*)
- les fichiers *env_meta.txt*, *map.sqlite* et *map_meta.txt* et les mettre dans le dossier du monde (*CartOSM_quartier/*).


## Dans le jeu
Lancer le jeu et, si necessaire, remettre la carte et les logos IGN a la verticale.
Si l'emplacement n'est pas adequate, deplacer l'ensemble (blocs d'obsidienne, carte et logos).
Si vous operez ces changements, il faudra noter les coordonnees du centre de la carte, du logo IGN et du logo IGNfab.

### Faire la prison
- Faire une prison au dos de la carto IGN

### Prendre les references
- Noter les coordonnees du point de spawn (face a la carte IGN ou ajustez-le)
- Noter les coordonnees du centre de la prison
- Noter les coordonnees des points opposes de la zone a proteger (carto IGN, prison, ...)


## Changer les coordonnees du spawn

- Modifier les coordonnees du spawn dans le fichier *worldmods/respawn/init.lua* (lignes 2 et 6).

- Modifier les coordonnees de la ligne 3 en y mettant les votres (a la place de "4,5,6")


## Configurer les accès

Dans le fichier *CartOSM_quartier/minetest.conf* , noter ou changer le mot de passe. Le numero de port peut aussi etre ajuste.


## Changer les coordonnees de la cartographie

- Si vous avez deplacez la carte IGN et les logos, modifier leurs coordonnees dans le fichier *worldmods/ignfab/init.lua*
	+ la ligne 54 > coordonnees du centre de la carte
	+ la ligne 55 > coordonnees du logo IGN
	+ la ligne 56 > coordonnees du logo IGNfab


## Definir la prison

- Modifier les coordonnees de la prison dans le fichier *worldmods/jail/init.lua*
	+ la ligne 7 > coordonnees du centre de la prison
	+ la ligne 27 > coordonnees du spawn


## Protéger la zone de spawn + prison

- Modifier les coordonnees de la zone dans le fichier *CartOSM_quartier/area.dat* 
	+  lignes 2 et 3 > attention l'ordre des coordonnees x et y est inverse !


## Archiver

Sauvegarder la version vierge de *CartOSM_quartier* en le dupliquant

